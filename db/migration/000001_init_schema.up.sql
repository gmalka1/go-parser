CREATE SEQUENCE IF NOT EXISTS vacancies_id_seq
MINVALUE 0
START 0
INCREMENT 1;

CREATE TABLE IF NOT EXISTS vacancies (
    id  bigint default nextval('vacancies_id_seq'::regclass),
    description VARCHAR(12000),
    max_sal bigint  default 0,
    min_sal bigint  default 0,
    publish_date DATE NOT NULL DEFAULT CURRENT_DATE
);

CREATE INDEX index_on_id ON vacancies(id);