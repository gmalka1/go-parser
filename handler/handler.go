package handler

// Package classification infoblog.
//
// Documentation of your project API.
//
//	Schemes:
//	- http
//	BasePath: /
//	Version: 1.0.0
//
//	Consumes:
//	- application/json
//
//	Produces:
//	- application/json
//
//
// swagger:meta

import (
	"encoding/json"
	"io"
	"net/http"
	model "pr1/repository/Model"
	"strconv"
	"text/template"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
)

const (
	swaggerTemplate = `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="//unpkg.com/swagger-ui-dist@3/swagger-ui-standalone-preset.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui-standalone-preset.js"></script> -->
    <script src="//unpkg.com/swagger-ui-dist@3/swagger-ui-bundle.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui-bundle.js"></script> -->
    <link rel="stylesheet" href="//unpkg.com/swagger-ui-dist@3/swagger-ui.css" />
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui.css" /> -->
	<style>
		body {
			margin: 0;
		}
	</style>
    <title>Swagger</title>
</head>
<body>
    <div id="swagger-ui"></div>
    <script>
        window.onload = function() {
          SwaggerUIBundle({
            url: "/public/swagger.json?{{.Time}}",
            dom_id: '#swagger-ui',
            presets: [
              SwaggerUIBundle.presets.apis,
              SwaggerUIStandalonePreset
            ],
            layout: "StandaloneLayout"
          })
        }
    </script>
</body>
</html>
`
)

type VacancyService interface {
	CreateVacancy(dto model.Vacancy) error
	GetVacancy(id int) (model.Vacancy, error)
	GetAllVacancies() ([]model.Vacancy, error)
	DeleteVacancy(id int) error
}

type Manager interface {
	Create(dto model.Vacancy) error
	GetById(id int) (model.Vacancy, error)
	GetList() ([]model.Vacancy, error)
	Delete(id int) error
}

type Handler struct {
	service VacancyService
}

func NewHandler(service VacancyService) *Handler {
	return &Handler{service: service}
}

func (h *Handler) IniRouter() http.Handler {
	r := chi.NewRouter()

	r.Use(middleware.Logger)
	r.Get("/vacancy/{vacancy}", h.GetVacancy)
	r.Post("/vacancy", h.CreateVacancy)
	r.Get("/vacancy", h.GetAllVacancy)
	r.Delete("/vacancy/{vacancy}", h.DeleteVacancy)

	r.Get("/swagger", swaggerUI)
	r.Get("/public/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/public/", http.FileServer(http.Dir("./public"))).ServeHTTP(w, r)
	})


	return r
}

func swaggerUI(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	tmpl, err := template.New("swagger").Parse(swaggerTemplate)
	if err != nil {
		return
	}
	err = tmpl.Execute(w, struct {
		Time int64
	}{
		Time: time.Now().Unix(),
	})
	if err != nil {
		return
	}
}

func (h *Handler) GetVacancy(w http.ResponseWriter, r *http.Request) {
	vacancyid := chi.URLParam(r, "vacancy")
	id, err := strconv.Atoi(vacancyid)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	v, err := h.service.GetVacancy(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	b, err := json.Marshal(v)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.Write(b)
	w.WriteHeader(200)
}

func (h *Handler) CreateVacancy(w http.ResponseWriter, r *http.Request) {
	b, err := io.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	var v model.Vacancy
	err = json.Unmarshal(b, &v)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = h.service.CreateVacancy(v)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.WriteHeader(200)
}

func (h *Handler) GetAllVacancy(w http.ResponseWriter, r *http.Request) {
	l, err := h.service.GetAllVacancies()
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	b, err := json.Marshal(l)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.Write(b)
	w.WriteHeader(200)
}

func (h *Handler) DeleteVacancy(w http.ResponseWriter, r *http.Request) {
	vacancyid := chi.URLParam(r, "vacancy")
	id, err := strconv.Atoi(vacancyid)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = h.service.DeleteVacancy(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.WriteHeader(200)
}