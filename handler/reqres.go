package handler

import model "pr1/repository/Model"

// swagger:route POST /vacancy vacancy vacancyAddRequest
// Добавление Вакансии
// responses:
//   200: vacancyAddResponse

// swagger:parameters vacancyAddRequest
type vacancyAddRequest struct {
	// ID of an item
	//
	// In:body
	Body model.Vacancy
}

// swagger:response vacancyAddResponse
type vacancyAddResponse struct {
	// in:body
	Body model.Vacancy
}

// swagger:route GET /vacancy/{id} vacancy vacancyGetRequest
// Получение питомца по id.
// responses:
//   200: vacancyGetResponse

// swagger:parameters vacancyGetRequest
type vacancyGetRequest struct {
	// ID of an item
	//
	// In:path
	ID string `json:"id"`
}

// swagger:response vacancyGetResponse
type vacancyGetResponse struct {
	// in:body
	Body model.Vacancy
}

// swagger:route GET /vacancy vacancy vacancyGetAllRequest
// Получить все вакансии
// responses:
//   200: vacancyGetAllResponse

// swagger:parameters vacancyGetAllRequest
type vacancyGetAllRequest struct {
}

// swagger:response vacancyGetAllResponse
type vacancyGetAllResponse struct {
	// in:body
	Body []model.Vacancy
}

// swagger:route DELETE /vacancy/{id} vacancy vacancyDeleteRequest
// Удаление вакансии
// responses:
//   200: vacancyDeleteResponse

// swagger:parameters vacancyDeleteRequest
type vacancyDeleteRequest struct {
	// ID of an Vacancy
	//
	// in:path
	ID string `json:"id"`
}

// swagger:response vacancyDeleteResponse
type vacancyDeleteResponse struct {
}
