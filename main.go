package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/go-redis/redis"
	_ "github.com/lib/pq"

	"github.com/PuerkitoBio/goquery"
	"github.com/tebeka/selenium"
	"github.com/tebeka/selenium/chrome"

	"pr1/handler"
	model "pr1/repository/Model"
	"pr1/repository/vacancyMongoManager"
	"pr1/repository/vacancyPostgresManager"
	"pr1/repository/vacancyRedisManager"
	"pr1/service"

	"github.com/joho/godotenv"
	_ "go.mongodb.org/mongo-driver/mongo"
)

type Manager interface {
	Create(dto model.Vacancy) error
	GetById(id int) (model.Vacancy, error)
	GetList() ([]model.Vacancy, error)
	Delete(id int) error
}

type VacancyService interface {
	CreateVacancy(dto model.Vacancy) error
	GetVacancy(id int) (model.Vacancy, error)
	GetAllVacancies() ([]model.Vacancy, error)
	DeleteVacancy(id int) error
}

const maxTries = 5

func main() {
	var (
		manager Manager
		serv VacancyService
		err error
	)

	err = godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	databaseToUse := os.Getenv("DATABASE_TO_USE")

	if databaseToUse == "mongo" {
		manager, err = vacancyMongoManager.NewMongoConnection("mongodb", "27017", "users")
		if err != nil {
			log.Fatalln(err)
		}
	} else if databaseToUse == "postgres" {
		manager, err = vacancyPostgresManager.NewManager("db", "5432", "mydb", "postgres", "postgres", "disable")
		if err != nil {
			log.Fatalln(err)
		}
	} else {
		log.Fatalf("Unknown command: %s", databaseToUse)
	}

	client := redis.NewClient(&redis.Options{
		Addr: "redis:6379",
	})
	defer client.Close()
	
	redisManager := vacancyRedisManager.NewRedisManager(manager, client)
	serv = service.NewService(redisManager)
	
	ParseChrome(serv)

	log.Println("Ready to connect")
	r := handler.NewHandler(serv)
	RunServer(":8080", r.IniRouter())
}

func ParseChrome(serv VacancyService) {
	// прописываем конфигурацию для драйвера
	caps := selenium.Capabilities{
		"browserName": "chrome",
	}

	chrCaps := chrome.Capabilities{
		W3C: true,
	}
	caps.AddChrome(chrCaps)

	var wd selenium.WebDriver
	var err error

	urlPrefix := "http://silenium:4444/wd/hub"

	i := 1
	for i < maxTries {
		wd, err = selenium.NewRemote(caps, urlPrefix)
		if err != nil {
			log.Println(err)
			i++
			continue
		}
		break
	}

	defer wd.Quit()

	page := 1         // номер страницы
	query := "golang" // запрос
	wd.Get(fmt.Sprintf("https://career.habr.com/vacancies?page=%d&q=%s&type=all", page, query))

	elem, err := wd.FindElement(selenium.ByCSSSelector, ".search-total")
	if err != nil {
		panic(err)
	}

	vacancyCountRaw, err := elem.Text()
	if err != nil {
		panic(err)
	}

	fmt.Println(vacancyCountRaw)
	elems, err := wd.FindElements(selenium.ByCSSSelector, ".vacancy-card__title-link")
	if err != nil {
		panic(err)
	}

	var links []string
	for i := range elems {
		var link string
		link, err = elems[i].GetAttribute("href")
		if err != nil {
			continue
		}
		links = append(links, "https://career.habr.com"+link)
	}

	for _, l := range links {
		resp, err := http.Get(l)
		if err != nil {
			panic(err)
		}
		var doc *goquery.Document
		doc, err = goquery.NewDocumentFromReader(resp.Body)
		if err != nil && doc != nil {
			panic(err)
		}
		dd := doc.Find("script[type=\"application/ld+json\"]")
		if dd == nil {
			panic("habr vacancy nodes not found")
		}
		ss := dd.First().Text()
		var v model.Vacancy
		err = json.Unmarshal([]byte(ss), &v)
		if err != nil {
			log.Fatalln(err)
		}
		err = serv.CreateVacancy(v)
		if err != nil {
			panic(err)
		}
	}
}

func RunServer(addr string, h http.Handler) {
	srv := &http.Server{
		Addr:    addr,
		Handler: h,
	}

	go srv.ListenAndServe()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutdown Server...")

	ctx, _ := context.WithTimeout(context.Background(), 1*time.Minute)
	srv.Shutdown(ctx)
	log.Println("Server exited")
}
