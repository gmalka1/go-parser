package model

import (
	"encoding/json"
	"errors"
)

type Vacancy struct {
	Context					string 					`json:"@context"`
	Type					string 					`json:"@type"`
	DatePosted				string	 				`json:"datePosted"`
	Title					string 					`json:"title"`
	Description				string 					`json:"description"`
	Ident 					identifier 				`json:"identifier"`
	ValidThrough 			string	 				`json:"validThrough"`
	HiringOrganization		hiringOrganization		`json:"hiringOrganization"`
	JobLocation				J						`json:"jobLocation"`
	JobLocationType			string					`json:"jobLocationType,omitempty"`
	EmploymentType			string					`json:"employmentType"`
	BaseSalary				salary					`json:"BaseSalary,omitempty"`
}

type J struct {
	J					*jobLocation				`json:",omitempty"`
	A					[]jobLocationSlice			`json:",omitempty"`
}

type jobLocation struct {
	Type					string 					`json:"@type"`
	Addr					address					`json:"address"`
}

type jobLocationSlice struct {
	Type					string 					`json:"@type"`
	Addr					string					`json:"address"`
}

func (r *J) UnmarshalJSON(b []byte) error {
	var AddrSlice jobLocation
	AddrString := make([]jobLocationSlice, 0)

	if err := json.Unmarshal(b, &AddrSlice); err == nil {
		r.J = &AddrSlice
		return nil
	} else if err = json.Unmarshal(b, &AddrString); err == nil {
		r.A = AddrString
		return nil
	}

	return errors.New("some error")
}

func (r J) MarshalJSON() ([]byte, error) {
	if r.A != nil {
		b, err := json.Marshal(r.A)
		return b, err
	} else {
		b, err := json.Marshal(r.J)
		return b, err
	}
}

type addressCountry struct {
	Type					string 					`json:"@type"`
	Name					string 					`json:"name"`
}

type address struct {
	Type					string 					`json:"@type"`
	StreetAddress			string					`json:"streetAddress"`
	AddressLocality			string					`json:"addressLocality"`
	AddressCountry			addressCountry			`json:"addressCountry"`
}

type hiringOrganization struct {
	Type					string 					`json:"@type"`
	Name					string 					`json:"name"`
	Logo					string 					`json:"logo"`
	SameAs					string 					`json:"sameAs"`
}

type identifier struct {
	Type					string 					`json:"@type"`
	Name					string 					`json:"name"`
	Value					string 					`json:"value"`
}

type salary struct {
	Type					string 					`json:"@type"`
	Currency				string					`json:"currency"`
	Value					value					`json:"value,omitempty"`
}

type value struct {
	Type					string 					`json:"@type"`
	MinValue				float64					`json:"minValue,omitempty"`
	MaxValue				float64					`json:"maxValue,omitempty"`
	UnitText				string					`json:"unitText"`
}