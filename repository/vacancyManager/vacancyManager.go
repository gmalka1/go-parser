package vacancyManager

import (
	"errors"
	model "pr1/repository/Model"
	"strconv"
)

type Manager interface {
	Create(dto model.Vacancy) error
	GetById(id int) (model.Vacancy, error)
	GetList() ([]model.Vacancy, error)
	Delete(id int) error
}

type VacancyManager struct {
	m map[int]model.Vacancy
}

func NewManager() Manager {
	return VacancyManager{make(map[int]model.Vacancy, 10)}
}

func (vm VacancyManager) Create(dto model.Vacancy) error {
	id, err := strconv.Atoi(dto.Ident.Value)
	if err != nil {
		return err
	}
	vm.m[id] = dto
	return nil
}

func (vm VacancyManager) GetById(id int) (model.Vacancy, error) {
	if v, ok := vm.m[id]; ok {
		return v, nil
	}
	return model.Vacancy{}, errors.New("doesn't contain element")
}

func (vm VacancyManager) GetList() ([]model.Vacancy, error) {
	if len(vm.m) == 0 {
		return nil, errors.New("empty list")
	}
	arr := make([]model.Vacancy, len(vm.m))

	for _, v := range vm.m {
		arr = append(arr, v)
	}

	return arr, nil
}

func (vm VacancyManager) Delete(id int) error {
	if _, ok := vm.m[id]; ok {
		delete(vm.m, id)
		return nil
	}

	return errors.New("doesn't contain element")
}
