package vacancyMongoManager

import (
	"context"
	"fmt"
	model "pr1/repository/Model"
	"strconv"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Manager interface {
	Create(dto model.Vacancy) error
	GetById(id int) (model.Vacancy, error)
	GetList() ([]model.Vacancy, error)
	Delete(id int) error
}

type manager struct {
	conn *mongo.Collection
}

func NewMongoConnection(url, port, database string) (Manager, error) {
	client, err := mongo.Connect(context.TODO(), options.Client().ApplyURI(fmt.Sprintf("mongodb://%s:%s", url, port)))
	if err != nil {
		return nil, err
	}
	conn := client.Database(database).Collection("users")

	return manager{conn: conn}, nil
}

func (m manager) Create(dto model.Vacancy) error {
	_, err := m.conn.InsertOne(context.Background(), dto)
	if err != nil {
		return err
	}

	return nil
}

func (m manager) GetById(id int) (model.Vacancy, error) {
	var v model.Vacancy

	s := strconv.Itoa(id)
	res := m.conn.FindOne(context.Background(), bson.M{
		"ident.value": s,
	})

	err := res.Decode(&v)
	if err != nil {
		return model.Vacancy{}, err
	}

	return v, nil
	
}

func (m manager) GetList() ([]model.Vacancy, error) {
	var v []model.Vacancy
	cur, err := m.conn.Find(context.Background(), bson.M{})
	if err != nil {
		return nil, err
	}

	cur.All(context.Background(), &v)
	return v, nil
}

func (m manager) Delete(id int) error {
	s := strconv.Itoa(id)
	_, err := m.conn.DeleteOne(context.Background(), bson.M{
		"ident.value": s,
	})
	if err != nil {
		return err
	}
	return nil
}
