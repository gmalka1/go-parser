package vacancyPostgresManager

import (
	"fmt"
	"log"
	model "pr1/repository/Model"
	"time"

	"github.com/jmoiron/sqlx"
)

type Manager interface {
	Create(dto model.Vacancy) error
	GetById(id int) (model.Vacancy, error)
	GetList() ([]model.Vacancy, error)
	Delete(id int) error
}

type manager struct {
	db *sqlx.DB
}

func NewManager(url, port, database, user, password, sslmode string) (Manager, error) {
	var (
		err error
		db	*sqlx.DB
	)
	for i := 0; i < 50; i++ {
		db, err = sqlx.Open("postgres", fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=%s",
			url, port, user, database, password, sslmode))
		if err != nil {
			log.Println("Connecting...")
		}
		time.Sleep(time.Second)
	}
	if err != nil {
		return nil, err
	}

	err = db.Ping()
	if err != nil {
		return nil, err
	}
	
	return manager{db: db}, nil
}

func (m manager) Create(dto model.Vacancy) error {
	t, err := time.Parse("2006-01-02", dto.DatePosted)
	if err != nil {
		return err
	}
	_, err = m.db.Exec("INSERT INTO vacancies(id, description, max_sal, min_sal, publish_date) VALUES($1,$2,$3,$4,$5)",
		dto.Ident.Value, dto.Description, dto.BaseSalary.Value.MinValue, dto.BaseSalary.Value.MaxValue, t)
	if err != nil {
		return err
	}

	return nil
}

func (m manager) GetById(id int) (model.Vacancy, error) {
	var v model.Vacancy
	err := m.db.QueryRow("SELECT * FROM vacancies WHERE id=$1", id).Scan(&v.Ident.Value, &v.Description, &v.BaseSalary.Value.MinValue, &v.BaseSalary.Value.MaxValue, &v.DatePosted)
	return v, err
}

func (m manager) GetList() ([]model.Vacancy, error) {
	vacansies := make([]model.Vacancy, 0, 10)
	rows, err := m.db.Query("SELECT * FROM vacancies")
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var v model.Vacancy
		err := rows.Scan(&v.Ident.Value, &v.Description, &v.BaseSalary.Value.MinValue, &v.BaseSalary.Value.MaxValue, &v.DatePosted)
		if err != nil {
			return nil, err
		}
		vacansies = append(vacansies, v)
	}
	return vacansies, nil
}

func (m manager) Delete(id int) error {
	_, err := m.db.Exec("DELETE * FROM vacancies WHERE id=$1", id)
	if err != nil {
		return err
	}
	return nil
}