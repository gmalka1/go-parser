package vacancyRedisManager

import (
	"encoding/json"
	"fmt"
	model "pr1/repository/Model"
	"time"

	"github.com/go-redis/redis"
)

type Manager interface {
	Create(dto model.Vacancy) error
	GetById(id int) (model.Vacancy, error)
	GetList() ([]model.Vacancy, error)
	Delete(id int) error
}

type manager struct {
	m Manager
	client *redis.Client
}

func NewRedisManager(m Manager, client *redis.Client) Manager {
	return manager{m: m, client: client}
}

func (m manager) Create(dto model.Vacancy) error {
	return m.m.Create(dto)
}

func (m manager) GetById(id int) (model.Vacancy, error) {
	result, err := m.client.Get(fmt.Sprintf("vacancy:%d", id)).Result()
	if err != nil {
		vacancy, err := m.m.GetById(id)

		if err != nil {
			return vacancy, err
		}

		json, err := json.Marshal(vacancy)
		if err != nil {
			return vacancy, err
		}
		err = m.client.Set(fmt.Sprintf("vacancy:%d", id), json, time.Hour * 4).Err()

		return vacancy, err
	} else {
		var v model.Vacancy

		err = json.Unmarshal([]byte(result), &v)
		if err != nil {
			return v, err
		}

		return v, nil
	}
}

func (m manager) GetList() ([]model.Vacancy, error) {
	return m.m.GetList()
}

func (m manager) Delete(id int) error {
	m.client.Del(fmt.Sprintf("vacancy:%d", id))
	return m.m.Delete(id)
}