package service

import model "pr1/repository/Model"

type Manager interface {
	Create(dto model.Vacancy) error
	GetById(id int) (model.Vacancy, error)
	GetList() ([]model.Vacancy, error)
	Delete(id int) error
}

type VacancyService interface {
	CreateVacancy(dto model.Vacancy) error
	GetVacancy(id int) (model.Vacancy, error)
	GetAllVacancies() ([]model.Vacancy, error)
	DeleteVacancy(id int) error
}

type vacancyService struct {
	m Manager
}

func NewService(m Manager) VacancyService {
	return vacancyService{m: m}
}

func (vs vacancyService) CreateVacancy(dto model.Vacancy) error {
	return vs.m.Create(dto)
}

func (vs vacancyService) GetVacancy(id int) (model.Vacancy, error) {
	return vs.m.GetById(id)
}

func (vs vacancyService) GetAllVacancies() ([]model.Vacancy, error) {
	return vs.m.GetList()
}

func (vs vacancyService) DeleteVacancy(id int) error {
	return vs.m.Delete(id)
}
